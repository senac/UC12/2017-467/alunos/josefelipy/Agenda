<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="../Header.jsp"/>


<% List<Usuario> lista = (List) request.getAttribute("lista"); %>
<% String mensagem = (String) request.getAttribute ("mensagem") ; %>



<% if (mensagem != null) { %>
<div class=" alert alert-danger ">
    
    <%= mensagem %>
    
    </div>
    
    <% } %>
<fieldset>
    
    <legend>Pesquisa de Usu�rios</legend>
    
    <form class="form-inline" action="./PesquisaUsuarioServlet">
        <div class="form-group" style="padding: 20px">
        <label for="textCodigo" style="margin-right: 10px">C�digo: </label> 
        <input id="id" name="codigo" class="form-control form-control-sm" id="textCodigo" type="text" />
    </div>
       
    <div class="form-group">
        <label for="textNome" style="margin-right: 10px">Nome: </label>
        <input id="nome" name="nome" class="form-control form-control-sm" id="textNome" type="text" />
    </div>
        <button type="submit" class="btn bg-default" style="margin-left: 10px">Pesquisar</button>
    
    </form>

</fieldset>

<hr/>

<table>
    <thead>
        <tr>
            
            <th>C�digo</th> <th>Nome</th>
            
        </tr>
        
    </thead>
    
    <% if (lista != null && lista.size() > 0) {
        for (Usuario u : lista ){
        %>
        <tr>
            <td><%= u.getId() %></td><td><%= u.getNome() %></td>
        </tr>
    <% }

}else{%>

<tr>
    <td colspan="2">N�o existem registros.</td>
</tr>
    
  <%}%>
    
    
</table>

<jsp:include page="../Footer.jsp"/>