<%@page import="java.util.List" %>
<%@page import="br.com.senac.agenda.model.Contato" %>
<jsp:include page="../Header.jsp"/>

<%
    Contato contato = (Contato) request.getAttribute("contato");
    String mensagens = (String) request.getAttribute("mensagens");
    String erro = (String) request.getAttribute("erro");

    
%>

<% if (mensagens != null) { %>
<div class=" alert alert-success">
    
    <%= mensagens %>
    
    
</div>
<% } %>


<% if ( erro != null) {%>
<div>
    <%= erro%>
    
</div>
    <%}%>
    
<form action="./CadastroContatoServlet" method="post">
    <div class="form-group">
        <label for="textCodigo" style="margin-right: 10px">C�digo:</label>
        <p><input name="codigo" type="text" id="codigo" class="form-control col-2" readonly=""/></p>
    </div>

    <hr/>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="textNome">Nome Completo:</label>
            <input name= "nome" type="text" class="form-control" id="nome" >
        </div>  
    </div>

    <div class="form-row">
        <div class="form-group col-2">
            <label for="textTelefone">Telefone:</label>
            <input name="telefone" type="text" class="form-control" id="telefone" >
        </div>

        <div class="form-group col-md-2">
            <label for="textCelular">Celular:</label>
            <input name="celular" type="text" class="form-control" id="celular" >
        </div>
    </div>  

    <div class="form-row">
        <div class="form-group col-6">
            <label for="textEndere�o">Endere�o:</label>
            <input name="endereco"   type="text" class="form-control" id="endere�o" >
        </div>

        <div class="form-group col-4">
            <label for="textCep">CEP:</label>
            <input name="cep" type="text" class="form-control" id="cep" >
        </div>

        <div class="form-group col-2">
            <label for="textNumero">Numero:</label>
            <input name="numero" type="text" class="form-control" id="numero" >
        </div>
    </div>
    <div class="form-row">

        <div class="form-group col-4">
            <label for="textBairro">Bairro:</label>
            <input name="bairro"  type="text" class="form-control" id="bairro" >      
        </div>

        <div class="form-group col-4">
            <label for="textCidade">Cidade:</label>
            <input name="cidade"  type="text" class="form-control" id="cidade" >      
        </div>

        <div class="form-group col-4">
            <label for="textEstado">Estado:</label>
            <select type="text" id="estado" name="estado" class="form-control">
                <option selected>UF</option>
                <option>ES</option>
                <option>SP</option>
                <option>RJ</option>
                <option>MG</option>
                <option>BH</option>
            </select>
        </div> 
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="textEmail">Email:</label>
            <input name="email" type="text" class="form-control" id="email" >
        </div>  
    </div>
    <hr/>

    <div>
        <button type="submit"  class="btn btn-success">Salvar</button>
        <button type="reset"  class="btn btn-danger">Cancelar</button>
    </div>
</form>

<jsp:include page="../Footer.jsp"/>
