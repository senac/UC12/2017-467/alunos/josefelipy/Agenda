/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "SalvarUsuarioServlet", urlPatterns = {"/Usuario/SalvarUsuarioServlet"})
public class SalvarUsuarioServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
   
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String codigo = request.getParameter("id");
        
        String erro = null;
                
        
        try {

            Usuario usuario = null ; 
            int id;
            try {
                id = Integer.parseInt(codigo);
            } catch (NumberFormatException e) {
                id = 0;
            }
           

            UsuarioDAO dao = new UsuarioDAO();

             usuario = dao.get(id);
           
            request.setAttribute("usuario", usuario);
           
        } catch (Exception ex) {
            erro = "Erro ao salvar usuario.";
            request.setAttribute("erro", erro);
        }
        RequestDispatcher dispatcher =  request.getRequestDispatcher("./GerenciarUsuario.jsp");
        
        dispatcher.forward(request, response);
    
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
  
        String id = request.getParameter("id");
        String nome = request.getParameter("nome");
        String senha = request.getParameter("senha");
        String mensagens = null;   
        String erro = null;
                
        
        try {

            Usuario usuario = new Usuario();
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }
            usuario.setId(codigo);
            usuario.setNome(nome);
            usuario.setSenha(senha);

            UsuarioDAO dao = new UsuarioDAO();

            dao.salvar(usuario);
            mensagens = usuario.getNome() + " salvo com sucesso!";
            request.setAttribute("usuario", usuario);
            request.setAttribute("mensagens", mensagens);
        } catch (Exception ex) {
            erro = "Erro ao salvar usuario.";
            request.setAttribute("erro", erro);
        }
        RequestDispatcher dispatcher =  request.getRequestDispatcher("./PesquisaUsuario.jsp");
        
        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
