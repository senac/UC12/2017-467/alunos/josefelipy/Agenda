/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ContatoDAO extends DAO<Contato> {

    @Override
    public void salvar(Contato contato) {

        Connection connection = null;

        try {

            String query;

            if (contato.getCodigo() == 0) {
                query = "INSERT INTO contato (nome , telefone, celular, cep, endereco, numero, bairro, cidade, estado, email) values (? , ? , ? , ? , ? , ? , ? ,? , ? , ? );";

            } else {
                query = "update contato set nome = ? , telefone = ? , celular = ? , cep = ? , endereco = ?, numero = ? , bairro = ? , cidade = ? , estado = ? , email = ? where id = ? ;";
            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getTelefone());
            ps.setString(3, contato.getCelular());
            ps.setString(4, contato.getCep());
            ps.setString(5, contato.getEndereco());
            ps.setString(6, contato.getNumero());
            ps.setString(7, contato.getBairro());
            ps.setString(8, contato.getCidade());
            ps.setString(9, contato.getEstado());
            ps.setString(10, contato.getEmail());

            if (contato.getCodigo() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setCodigo(rs.getInt(1));
            } else {
                ps.setInt(11, contato.getCodigo());
                ps.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("FALHAAAAAAAAAAAAA");
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println("Erro de Conexao");
            }
        }

    }

    @Override
    public void deletar(Contato objeto) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Contato> listar() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Contato get(int id) {
        Contato contato = null;
        Connection connection = null;
        String query = "SELECT * FROM contato where id = ?";
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                contato = new Contato();
                //talvez seja codigo no lugar de id
                contato.setCodigo(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setBairro(rs.getString("bairro"));
                contato.setCelular(rs.getString("celular"));
                contato.setCep(rs.getString("cep"));
                contato.setCidade(rs.getString("cidade"));
                contato.setEmail(rs.getString("email"));
                contato.setEstado(rs.getString("estado"));
                contato.setNumero(rs.getString("numero"));
                contato.setEndereco(rs.getString("endereco"));

            }

        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão.");
            }
        }

        return contato;
    }



public Contato getByName(String name) {
        
       Contato contato = null;
       Connection connection = null;
       String query = "SELECT * FROM contato where nome = ?" ;
       try {
           connection = Conexao.getConnection();
           PreparedStatement ps = connection.prepareStatement(query);
           ps.setString(1, name);
           ResultSet rs = ps.executeQuery();
           if (rs.first()){
               contato = new Contato();
               //talvez seja codigo no lugar de id
               contato.setCodigo(rs.getInt("id"));
               contato.setNome(rs.getString("nome"));
               contato.setTelefone(rs.getString("telefone"));
               contato.setBairro(rs.getString("bairro"));
               contato.setCelular(rs.getString("celular"));
               contato.setCep(rs.getString("cep"));
               contato.setCidade(rs.getString("cidade"));
               contato.setEmail(rs.getString("email"));
               contato.setEstado(rs.getString("estado"));
               contato.setNumero(rs.getString("numero"));
               contato.setEndereco(rs.getString("endereco"));           
           
           }
           
       } catch (Exception ex) {
           System.out.println("Erro ao executar a consulta");
           ex.printStackTrace();
           
       } finally {
              try {
                  connection.close();
              } catch (SQLException ex) {
                  System.out.println("Falha ao fechar conexão."); 
              }
       }
        
       return contato;
    }
    
    
    public List<Contato> getByFiltro (Integer id, String name, String estado) {
        
        List<Contato> lista = new ArrayList<>();
        Connection connection = null;
        try {
            
        StringBuilder sb = new StringBuilder(" SELECT * FROM CONTATO where 1 = 1");
        if (id != null) {
        sb.append(" and codigo = ?");
            
        }
        
        if (name != null && !name.trim().isEmpty()){
        sb.append(" AND NOME LIKE ?");
        }
        
        if (estado != null && !estado.isEmpty()){
         sb.append(" and estado = ? ");
        }
        
        connection = Conexao.getConnection();
        PreparedStatement ps = connection.prepareStatement(sb.toString());
        int index = 0;
        if (id != null) {
            ps.setInt(++index, id);
        }
        
        if (name != null && !name.trim().isEmpty()) {
            ps.setString(++index, "%" + name + "%");
        }
        if (estado != null && !estado.isEmpty()) {
            ps.setString(++index, estado);
        }
        
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            Contato contato = new Contato();
            contato.setCodigo(rs.getInt("codigo"));
            contato.setNome(rs.getString("nome"));
            contato.setTelefone(rs.getString("telefone"));
            contato.setBairro(rs.getString("bairro"));
            contato.setCelular(rs.getString("celular"));
            contato.setCep(rs.getString("cep"));
            contato.setCidade(rs.getString("cidade"));
            contato.setEmail(rs.getString("email"));
            contato.setEstado(rs.getString("estado"));
            contato.setNumero(rs.getString("numero"));
            contato.setEndereco(rs.getString("endereco"));           
            lista.add(contato);

        }
        
    }catch (Exception ex) {
            System.out.println("Erro ao realizar consulta");
            ex.printStackTrace();
    } finally {
        try {
            connection.close();
            
        } catch (SQLException ex){
            System.out.println("Falha ao fechar conexao...");
        }
     }
    
        return lista;
    }
}  
    
    
    

