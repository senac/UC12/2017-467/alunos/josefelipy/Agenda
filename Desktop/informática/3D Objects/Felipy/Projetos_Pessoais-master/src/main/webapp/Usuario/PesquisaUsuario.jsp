<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="../Header.jsp"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<script type="text/javascript">
    

function deletar(id , value){

var codigo = $('#codigo').val() ; 

var nome = $('#nome').val() ; 


$('#btnDeletar').attr('href','./DeletarUsuarioServlet?codigo=' + id + '&codigoForm='  + codigo + '&nomeForm=' + nome );
$('#usuario').text(value);

        
}

    
    
</script>
    
    



<% List<Usuario> lista = (List) request.getAttribute("lista"); %>
<% String mensagens = (String) request.getAttribute("mensagens"); %>



<% if (mensagens != null) {%>
<div class=" alert alert-danger ">

    <%= mensagens%>

</div>

<% } %>
<fieldset>

    <legend>Pesquisa de Usu�rios</legend>

    <form class="form-inline" action="./PesquisaUsuarioServlet">
        <div class="form-group" style="padding: 20px">
            <label for="textCodigo" style="margin-right: 10px">C�digo: </label> 
            <input id="codigo" name="codigo" class="form-control form-control-sm" id="textCodigo" type="text" />
        </div>

        <div class="form-group">
            <label for="textNome" style="margin-right: 10px">Nome: </label>
            <input id="nome" name="nome" class="form-control form-control-sm" id="textNome" type="text" />
        </div>
        <button type="submit" class="btn btn-dark"" style="margin-left: 10px"> <i class="fa fa-user" aria-hidden="true"></i> Pesquisar</button> 
        <a href="./GerenciarUsuario.jsp" class="btn btn-success" style="margin-left: 10px" > <i class="fa fa-user-plus" aria-hidden="true"></i> Adicionar  <a/> 

    </form>

</fieldset>

<hr/>

<table class="table table-hover table-dark" >
    <thead>
        <tr>

            <th>C�digo</th> <th>Nome</th> <th> Editar </th>

        </tr>

    </thead>

    <% if (lista != null && lista.size() > 0) {
            for (Usuario u : lista) {
    %>
    <tr>
        <td><%= u.getId()%></td><td><%= u.getNome()%></td>  

        <td style="width: 120px">  
             <a href="./SalvarUsuarioServlet?id=<%= u.getId()%>">
                <img src="../resources/imagens/Edit.png">
            </a>
            
            <a  data-toggle="modal" data-target="#exampleModal" onclick="deletar(<%=u.getId() %> ,'<%=u.getNome() %>' );">
                <img src="../resources/imagens/Delete.png">
            </a>
           
        </td>
    </tr>
    <% }

    } else {%>

    <tr>
        <td colspan="2">N�o existem registros.</td>
    </tr>

    <%}%>


</table>


<a data-toggle="modal" data-target="#exampleModal">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel" style="color: black">Aten��o!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="color: black">
                    Voc� tem certeza que deseja apagar <span id="usuario"></span> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> N�o. </button>
                    <a  id="btnDeletar"  href="" class="btn btn-success"> Confirmar <a/>
                </div>
            </div>
        </div>
    </div>
</a>


<jsp:include page="../Footer.jsp"/>